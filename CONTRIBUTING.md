# Contributing

Anyone is welcome to fork or clone my `dockerapps` repo, and make any changes
they please, as long as they remain
[within the parameters of the license](https://gitlab.com/rbprogrammer/dockerapps/blob/master/LICENSE).

That said, since this project maintains my own personal development I may
selfishly only accept Merge Requests that appear to benefit my own productivity.
