FROM ubuntu:20.04
LABEL maintainer "Michael Durso Jr. <rbprogrammer@gmail.com>"

WORKDIR /root

# Setup sudo access.
# ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends sudo
RUN echo "Set disable_coredump false" >>/etc/sudo.conf
ENV USER devenv
RUN echo "${USER} ALL=(ALL) NOPASSWD:ALL" >/etc/sudoers.d/${USER}

# Set up the run user.
ENV HOME /home/${USER}
RUN useradd --home-dir ${HOME} --create-home --password '!' ${USER}
USER ${USER}
WORKDIR ${HOME}

# Install generic system tools.
RUN sudo DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        apt-file \
        ca-certificates \
        curl \
        firefox \
        git \
        gnupg \
        man \
        ssh \
        tmux \
        tree \
        vim \
    && sudo apt-file update

# Install tools needed to develop C/C++ projects.
RUN sudo apt-get install -y --no-install-recommends \
        build-essential

# Create a directory to install the latest software.
RUN mkdir -pv /opt \
    && sudo chown ${USER}:${USER} /opt

# TODO Install tools needed to develop Java projects.
#RUN sudo apt-get install -y --no-install-recommends

# Install tools needed to develop NodeJS projects.
RUN sudo apt-get install -y --no-install-recommends \
        nodejs \
        npm

# Install tools needed to develop Python projects.
RUN sudo apt-get install -y --no-install-recommends \
        python3 \
        python3-dev \
        python3-pip
RUN sudo -H pip3 install --upgrade pip \
        dfmpy \
        IPython \
        termcolor \
        virtualenv \
        xdgenvpy

# Install tools needed to develop Ruby projects.
RUN sudo apt-get install -y --no-install-recommends ruby-full
RUN gpg --keyserver hkp://pool.sks-keyservers.net \
        --recv-keys \
            409B6B1796C275462A1703113804BB82D39DC0E3 \
            7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN curl -sSL https://get.rvm.io | bash -s stable --ruby
ENV PATH="${HOME}/rvm/bin:${PATH}"
RUN sudo gem install \
        bundler \
        jekyll \
        rails

# Install tools needed to use MongoDB.
RUN cd /opt \
    && curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1804-4.2.3.tgz \
    && tar -xvzf mongodb-*.tgz

# Fix for high-DPI monitors.
#RUN echo "Xft.dpi: 192" >${HOME}/.Xdefaults

# Ensure Git is configured.
RUN mkdir -pv ${HOME}/.config/git \
    && cd ${HOME}/.config/git \
    && touch config \
    && git config --global user.name 'Michael Durso Jr.' \
    && git config --global user.email 'rbprogrammer@gmail.com'

# Generate a new SSH key.
RUN echo | ssh-keygen -b 2048

# Install dotfiles.
# RUN cd /opt \
#     && git clone git@gitlab.com:rbprogrammer/dotfiles.git \
#     && dfmpy init \
#     && cat ~/.config/dfmpy/config.ini \
#     && dfmpy sync -vvf

# Install tools needed to run Jetbrains IDEs.
RUN sudo apt-get install -y --no-install-recommends \
        libasound2 \
        libatk1.0-0 \
        libatk-bridge2.0-0 \
        libdbus-1-3 \
        libfontconfig1 \
        libgl1 \
        libglib2.0-0 \
        libnss3 \
        libx11-6 \
        libx11-xcb1 \
        libxcb-keysyms1 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6

# Install user dev tools and the Jetbrains Toolbox.
ENV APPDIR ${HOME}/.local/share/jetbrains-toolbox
RUN mkdir -pv ${HOME}/.local/share \
    && cd ${HOME}/.local/share \
    && curl -LO https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.17.7005.tar.gz \
    && curl -LO https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.17.7005.tar.gz.sha256 \
    && sha256sum -c *.sha256 \
    && tar -xvzf jetbrains-toolbox-*.tar.gz \
    && ./jetbrains-toolbox-*/jetbrains-toolbox --appimage-extract \
    && mv -v squashfs-root jetbrains-toolbox \
    && rm -rfv jetbrains-toolbox-*

# Fix the XDG_RUNTIME_DIR error AppRun vomits out.
ENV XDG_RUNTIME_DIR /tmp/runtime-${USER}
RUN mkdir -pv ${XDG_RUNTIME_DIR} \
    && chmod -c 700 ${XDG_RUNTIME_DIR}

# Install the custom scripts.  Note that if you add more files to the includes/
# directory then they will not show up in a new container if the "home" volume
# is pre-existing.
RUN mkdir -pv ${HOME}/.local/bin
COPY includes/ ${HOME}/.local/bin
RUN sudo chown -Rc ${USER}:${USER} ${HOME}/.local/bin
RUN ls -al ${HOME}/.local/bin/
ENV PATH="${HOME}/.local/bin:${PATH}"

# The container should always be ran in the terminal, so no ENTRYPOINT/CMD here.
# The post-start script will tell the user an error occurred if the container
# was started as an application, and not as a console.
WORKDIR ${HOME}
# ENTRYPOINT ["/bin/bash"]
# CMD [""]
