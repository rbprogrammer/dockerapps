# dockerfiles

These project enables better containerization and encapsulation of apps to help
keep my main development machine free from clutter.  Each app is installed into
a Docker container, and ran from there.  Thus all of the app's dependencies are
encapsulated into the app specific container.  Should I no longer need that app
I can simply delete the containers and images without having to worry about
lingering dependencies on my system.

Thos project also helps document exactly what is needed for each app.  For
example, when I hop to a new distro it should be easy to get set up with all of
my favorite and most frequently used apps as soon as quickly.

To get started, simply clone the repo and add the `bin` directory to your
`$PATH`.

```bash
git clone git@gitlab.com:rbprogrammer/dockerapps.git
cd dockerapps/bin
export PATH="$(pwd):${PATH}"
```

## Starting and Stopping Apps

Starting an app is as simple as just running the `start` subcommand.

```bash
dockerapps start gimp
dockerapps start spotify
dockerapps start vscode
```

Stopping an app is just as simple, but with the `stop` subcommand.

```bash
dockerapps stop gimp
dockerapps stop spotify
dockerapps stop vscode
```

Starting and stopping apps are actually more complicated than just running a new
Docker container.  Since each container is a running instance of an image, if an
app's image is not yet built the the `start` command will build the image then
run a new container if the app is not already running.

While stopping an app merely kills the app then deletes the container.  The
image is cached on the system for quick reloading later.

## Cleaning and Rebuilding Apps

Should the app's Dockerfile change, or if the image somehow got corrupted, you
can delete everything pertaining to that app and rebuild a new image.

The `clean` subcommand is used to delete all running containers and pre-built
images for an app.

```bash
dockerapps clean gimp
dockerapps clean spotify
dockerapps clean vscode
```

The `rebuild` subcommand can be used to call `docker build ...` again on the
app's Dockerfile.  This allows Docker to rebuild an image starting from the
first modified command in the Dockerfile.  This is useful when only a small
change has been made to the Dockerfile and you do not need to wait to rebuild
everything from scratch.

```bash
dockerapps rebuild gimp
dockerapps rebuild spotify
dockerapps rebuild vscode
```

It should be pointed out that even though a `clean` and/or `rebuild` are
intended to help start anew, they do not delete any
[Docker Volumes](https://docs.docker.com/storage/volumes/) associated with the
app.  Should an app create and/or use a volume in their `init.sh` script, the
`[clean|rebuild]` subcommands will not touch  those volumes.  This was done
delibrately as the purpose of Docker Volumes is to provide data persistence, and
I did not want to accidentally delete the data the app specifically wants to
persist.  To clean up these volumes you must delete them explicitly through the
`docker volume` command.

```bash
# Find the app's volume with:
docker volume ls

# Delete the volume with:
docker volume rm -f <VOL_NAME>
```

## Testing Apps

Lastly, to provide some level of testing, the `dockerapps` command implements a
`test` subcommand where it merely builds each app's image.  This is essentially
used for Gitlab Continuous Integration (CI) to help build up an arbitrarly level
of confidence that the `dockerapps` script and all app Dockerfiles work as
expected.

Again, the `test` subcommand merely builds images.  It does not actually run any
apps, and thus does not testing the "_container_" side of Docker things (as
opposed to the "_image_" side of things).

```bash
dockerapps test gimp
dockerapps test spotify
dockerapps test vscode
```

## Adding New Apps

The basic file structure for a new dockerapp is:

```
`-- src
    `-- newapp
        |-- includes
        |   |-- file1
        |   |-- file2
        |   `-- fileN
        |-- Dockerfile
        |-- post-build
        |-- post-start
        |-- pre-build
        `-- pre-start
```

* Everything goes under the `src` directory.
* The next sub-directory is the name of the dockerapp.
* Under the new dockerapp's directory there are a number of files, both optional
  and required.
    * The `includes` directory is optional and includes any extra files that
      will be built into the image.  The dockerapp's Dockerfile will need to
      explicitly copy the files into the image and drop them into the desired
      installation location.
    * `Dockerfile` is the dockerfile that builds the dockerapp.
    * `pre-build` is a bash script sourced before building the image.
    * `post-build` is a bash script sourced after building the image.
    * `pre-start` is a bash script sourced before starting the container.
    * `post-start` is a bash script sourced after starting the container.

It is important to know that the `pre-*` scripts will be sourced so that any
environment variables they define can be used within the Dockerfiles.
Additionally, anything they write to stdout will be captured and used as
arguments into the `docker image build ...` and `docker container run ...`
commands, respectively.

The `post-*` scripts are similar in that they are sourced.  But as they are
executed after the image is built, or a container is started, any environment
variables they define will not be used anywhere.  Output from the `post-*`
scripts are also discarded.  The purpose of these scripts is to perform any
clean-up of resources, for example ephemeral volumes that may not be used across
runs.
